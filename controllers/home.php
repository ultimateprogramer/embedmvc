<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Home_Controller extends Controller {
    public function action_index()
    {
        View::Forge("welcome", array("message" => "This message is dynamically generated from the controller"));
    }
}
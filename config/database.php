<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Database_Config {
    public $config = array(
        "enabled" => false,
        "driver" => "mysql",
        "server" => "",
        "username" => "",
        "password" => "",
        "database_name" => ""
    );
}

<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Database_Config {
    public $config = array(
        "enabled" => true,
        "driver" => "mysql",
        "server" => "localhost",
        "username" => "root",
        "password" => "root",
        "database_name" => "sampledb",
    );
}

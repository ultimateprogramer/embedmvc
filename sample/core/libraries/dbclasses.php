<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Public_DB_Objects
{
    static public $db_driver = null;
}

class Where_Condition
{
    const CONDITION_AND = "AND";
    const CONDITION_OR = "OR";

    public $condition_type;
    public $condition_field;
    public $condition;
}
 
class DB_Field
{
    public $field_name;
    public $field_value;
}
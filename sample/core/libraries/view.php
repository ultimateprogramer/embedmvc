<?php if(!defined('EMBEDMVC')) die("No direct script access");

class View {
    public static function Forge($view, $view_data = array())
    {
        foreach($view_data as $key => $val) {
            global ${$key};
            ${$key} = $val;
        }

        require_once(VIEWPATH.$view.".php");
    }
}
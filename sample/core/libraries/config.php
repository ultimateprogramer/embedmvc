<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Config {
    public static function Load($config_file)
    {
        require_once(CONFIGPATH.$config_file.".php");

        $config_class_name = ucfirst($config_file)."_Config";
        $config_object = new $config_class_name;
        return $config_object->config;
    }
}
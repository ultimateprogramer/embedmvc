<?php if(!defined('EMBEDMVC')) die("No direct script access");
 
class Response {

    private static function get_action_function_name($action)
    {
        return "action_".$action;
    }

    public static function Execute($controller = null, $action = null, $parameters = array())
    {
        $default_action = Config::Load("defaultaction");

        if($controller != null) {
            require_once(CONTROLLERPATH.$controller.".php");

            $controller_class_name = ucfirst($controller)."_Controller";
            $controller_object = new $controller_class_name;

            if($action != null) {
                $action = self::get_action_function_name($action);
                $controller_object->$action($parameters);

                return;
            }

            $default_config_action = self::get_action_function_name($default_action["action"]);

            $controller_object->$default_config_action($parameters);

            return;
        }

        $default_config_controller = $default_action["controller"];

        require_once(CONTROLLERPATH.$default_config_controller.".php");

        $controller_class_name = ucfirst($default_config_controller)."_Controller";
        $controller_object = new $controller_class_name;

        if($action != null) {
            $action_function_name = self::get_action_function_name($action);
            $controller_object->$action_function_name($parameters);

            return;
        }

        $default_config_action = self::get_action_function_name($default_action["action"]);

        $controller_object->$default_config_action($parameters);

        return;
    }
}

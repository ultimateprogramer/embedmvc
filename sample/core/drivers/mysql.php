<?php if(!defined('EMBEDMVC')) die("No direct script access");
 
class Mysql_Driver {
    protected $db_object;

    // Initialize the database connection

    public function __construct()
    {
        $config = Config::Load("database");
        $this->db_object = mysql_connect($config["server"], $config["username"], $config["password"]);

        if($this->db_object) {
            mysql_select_db($config["database_name"], $this->db_object);
        }
    }

    // Close the database connection

    public function __destruct()
    {
        if($this->db_object) {
            mysql_close($this->db_object);
        }
    }

    // Private functions

    private function build_condition_string($first_condition = false, $condition)
    {
        if($first_condition) {
            return "WHERE ".$condition->condition_field." = '".mysql_real_escape_string($condition->condition)."'";
        }
        else {
            return $condition->condition_type." ".$condition->condition_field." = '".mysql_real_escape_string($condition->condition)."'";
        }
    }

    // Public functions

    public function db_select($table, $fields = "*", $conditions = array())
    {
        $mysql_query_string = "SELECT ".$fields." FROM ".$table." ";

        $first_condition = true;

        foreach($conditions as $condition) {
            $mysql_query_string.= $this->build_condition_string($first_condition, $condition);

            $first_condition = false;
        }

        $select_result = mysql_query($mysql_query_string, $this->db_object);

        if($select_result) {
            $result = array();

            while($row = mysql_fetch_array($select_result)) {
                $result[] = $row;
            }

            return $result;
        }

        return false;
    }

    public function db_insert($table, $fields = array())
    {
        $mysql_query_string = "INSERT INTO ".$table." ";

        $fields_string = "";
        $values_string = "";

        foreach($fields as $key => $value) {
            $fields_string.= "`".$key."`,";
            $values_string.= "'".$value."',";
        }

        $fields_string = rtrim($fields_string, ",");
        $values_string = rtrim($values_string, ",");

        $mysql_query_string.= "(".$fields_string.") VALUES (".$values_string.")";

        $insert_result = mysql_query($mysql_query_string, $this->db_object);

        return $insert_result;
    }

    public function db_update($table, $fields = array(), $conditions = array())
    {
        $mysql_query_string = "UPDATE ".$table." ";

        $first_field = true;

        foreach($fields as $field) {
            if($first_field) {
                $mysql_query_string.="SET `".$field->field_name."` = '".mysql_real_escape_string($field->field_value)."'";
            }
            else {
                $mysql_query_string.=", `".$field->field_name."` = '".mysql_real_escape_string($field->field_value)."'";
            }

            $first_field = false;
        }

        $first_condition = true;

        foreach($conditions as $condition) {
            $mysql_query_string.= $this->build_condition_string($first_condition, $condition);

            $first_condition = false;
        }

        $update_result = mysql_query($mysql_query_string, $this->db_object);

        return $update_result;
    }

    public function db_delete($table, $conditions = array())
    {
        $mysql_query_string = "DELETE FROM ".$table." ";

        $first_condition = true;

        foreach($conditions as $condition) {
            $mysql_query_string.= $this->build_condition_string($first_condition, $condition);

            $first_condition = false;
        }

        $delete_result = mysql_query($mysql_query_string, $this->db_object);

        return $delete_result;
    }
}

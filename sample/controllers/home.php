<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Home_Controller extends Controller {
    public function action_index()
    {
        $sample_model = new Model("sampletable", array("sampleid"));
        $sample_model->append(array("sample_id" => 1, "sample_message" => "Message 1"));
        $sample_model->append(array("sample_id" => 2, "sample_message" => "Message 2"));

        $records_from_db = $sample_model->get();
        
        View::Forge("welcome", array("message" => "This message is dynamically generated from the controller",
                               "records_from_db" => $records_from_db));
    }
}
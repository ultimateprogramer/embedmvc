<?php if(!defined('EMBEDMVC')) die("No direct script access");

class Model {

    protected $db_entity;
    protected $pk_fields = array();

    public function __construct($entity, $key_fields = array())
    {

        // Entity data

        $this->db_entity = $entity;
        $this->pk_fields = $key_fields;
    }

    public function get_all()
    {
        return Public_DB_Objects::$db_driver->db_select($this->db_entity);
    }

    public function get($fields = "*", $conditions = array())
    {
        return Public_DB_Objects::$db_driver->db_select($this->db_entity, $fields, $conditions);
    }

    public function append($fields = array())
    {
        return Public_DB_Objects::$db_driver->db_insert($this->db_entity, $fields);
    }

    public function update($values = array(), $conditions = array())
    {
        return Public_DB_Objects::$db_driver->db_update($this->db_entity, $values, $conditions);
    }

    public function delete($conditions = array())
    {
        return Public_DB_Objects::$db_driver->db_delete($conditions);
    }
}
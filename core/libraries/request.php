<?php if(!defined('EMBEDMVC')) die("No direct script access");
 
class Request {
    public static function get($var_name)
    {
        if(array_key_exists($var_name, $_GET)) {
            return $_GET[$var_name];
        }

        return false;
    }

    public static function post($var_name)
    {
        if(array_key_exists($var_name, $_POST)) {
            return $_POST[$var_name];
        }

        return false;
    }
}

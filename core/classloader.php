<?php if(!defined('EMBEDMVC')) die("No direct script access");

function load_php_in_directory($dir_path)
{
    $handle = opendir($dir_path);
    while (false !== ($file_name = readdir($handle))){

        $extension = strtolower(substr(strrchr($file_name, '.'), 1));

        // Include the file if it is a .php

        if($extension == 'php') {
            require_once($dir_path.$file_name);
        }
    }
}
 

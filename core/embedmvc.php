<?php

// Global constants

define("EMBEDMVC", true);

define("COREPATH", dirname(__FILE__).DIRECTORY_SEPARATOR);
define("CORELIBPATH", dirname(__FILE__)."/libraries/");

define("CONTROLLERPATH", dirname(__FILE__)."/../controllers/");
define("VIEWPATH", dirname(__FILE__)."/../views/");
define("CONFIGPATH", dirname(__FILE__)."/../config/");
define("DRIVERPATH", COREPATH."/drivers/");
define("MODELPATH", dirname(__FILE__)."/../models/");
define("LIBPATH", dirname(__FILE__)."/../libraries/");
define("HELPERPATH", dirname(__FILE__)."/../helpers/");

// Get the folder loader

require_once(COREPATH."classloader.php");

// Load the standard configuration items

require_once(CORELIBPATH."config.php");

// Load required helpers and libraries

$helpers = Config::Load("helpers");
$libraries = Config::Load("libraries");

foreach($helpers as $helper) {
    require_once(HELPERPATH.$helper);
}

foreach($libraries as $library) {
    require_once(LIBPATH.$library);
}

require_once(CORELIBPATH."dbclasses.php");

$database = Config::Load("database");

if($database["enabled"]) {
    require_once(DRIVERPATH.$database["driver"].".php");
    $database_driver_class_name = ucfirst($database["driver"])."_Driver";
    Public_DB_Objects::$db_driver = new $database_driver_class_name;
}

// Load the rest of the core

require_once(CORELIBPATH."model.php");
require_once(CORELIBPATH."view.php");
require_once(CORELIBPATH."controller.php");
require_once(CORELIBPATH."request.php");
require_once(CORELIBPATH."response.php");
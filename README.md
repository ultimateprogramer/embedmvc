#EmbedMVC

* Version: 1.0 Beta 1
* [Website](http://ahmedmaawy.com/)

##Description

EmbedMVC is a PHP framework which allows you to embed MVC logic within any other framework or PHP application and extend it using an MVC construct.

##Why is this different.

I know there are a ton of MVC frameworks out there. This is unique. This framework can be "embeded" into an existing one (and may also be used as a stand-alone).

##Development Team

* Ahmed Maawy - Project Manager, Developer ([http://ahmedmaawy.com/](http://ahmedmaawy.com/))


##Example usage

	<?php

	// Include the core EmbedMVC files
	include("core/embedmvc.php");
	
	// Use Input::get to gain access to $_GET parameters
	Response::Execute(Input::get('controller'), Input::get('action'), array('param1' => 'Parameter 1 value'));
	
This means that you can customize the framework to work with any other platform, so long as you specify which controller and actions to work on.

##Config files

###database.php

Database configuration. Specify the driver to use and connection settings. At the moment only supports MySQL.

###defaultaction.php

Default controller and action if one is not specified manually.

###helpers.php

What helpers to load from the helpers directory.

###libraries.php

What libraries to load from the libraries directory.

##Brief sample on how to use the framework.

Please refer to the sample/ folder for a sample on how to do basic MVC data binding using a Model.